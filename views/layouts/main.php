<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<!--<link rel="icon" href="<?php echo Yii::getAlias('@web/images/favicon.ico'); ?>" />-->
	<link rel="icon" href="<?php echo Yii::getAlias('@web/images/logo_alamos_new.png'); ?>" />
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<style>
	
		.home-info {
			justify-content: initial;
			top: 50px;
		}
	
	</style>

<!-- PRE LOADER -->
     <section class="preloader">
          <div class="spinner">

               <span class="spinner-rotate"></span>
               
          </div>
     </section>


     <!-- GRID LINE -->
    <section class="grid">
         <div class="container">
            <div class="row">

              <div class="col-md-2 col-sm-2">
                <div class="grid-line"></div>
              </div>
              <div class="col-md-2 col-sm-2">
                <div class="grid-line"></div>
              </div>
              <div class="col-md-2 col-sm-2">
                <div class="grid-line"></div>
              </div>
              <div class="col-md-2 col-sm-2">
                <div class="grid-line"></div>
              </div>
              <div class="col-md-2 col-sm-2">
                <div class="grid-line"></div>
              </div>

            </div>
         </div>
    </section>


    <div class="menu-bg hidden"></div>
    <div class="menu-burger hidden">☰</div>

    <div class="menu-items">
       <div class="container">
         <div class="row">
         </div>
       </div>
    </div>


     <!-- HOME -->
     <section id="home">
        <div class="overlay"></div>
          <div class="container">
               <div class="row">

                    <div class="col-md-12 col-sm-12">
                         <div class="home-info">
							  
                              <!-- You can change the date time in init.js file -->
                              <div class="container">
							  
							  <div class="row">
							  
							  <img src='<?php echo Yii::getAlias("@web/images/logo.jpeg"); ?>' width='20%'>
                              <h1 style='color:black;'>Em breve...</h1>
                              <h2 style='color:black;'><i>Mas até lá, entre em contato conosco!</i></h2>
							  
								<div class='col-md-offset-2 col-md-8 contatos'>
									<h3 class='text-center'>Contatos</h3>
									<p><b>Telefone:</b> (61) 3246-6735</p>
									<p><b>Email:</b> <a href='mailto:atendimento@alamosseguros.com'>atendimento@alamosseguros.com</a></p>
								</div>
							</div>
							  <br />
							  <br />
							  <br />
							 <div class="row">
								<div class='col-md-offset-2 col-md-8'>
							   <div class="col-md-4 col-sm-4">
								 <address>
								   <h1><b>Brasília</b></h1>
								   <p>SBS Quadra 02, Bloco E - 5º andar</p>
								   <p>Ed. Prime Business Center</p>
								   <p>Asa Sul - Brasília - DF</p>
								   <p>CEP: 70.070-120</p>
								   <div class="google-map hidden">
									  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3839.0550907876936!2d-47.88598398477883!3d-15.801048889047072!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a3baf03d4f049%3A0x9dc40b5db8f2b14b!2s%C3%81lamos+Seguros!5e0!3m2!1sen!2sbr!4v1550689746348" allowfullscreen></iframe>
								   </div>
								 </address>
							   </div>
							   
							   <div class="col-md-4 col-sm-4">
								 <address>
								   <h1><b>Rio de Janeiro</b></h1>
								   <p>Rua Sete de Setembro 99, 18º andar</p>
								   <p>Centro - RJ</p>
								   <p>CEP: 20.050-005</p>
								 </address>
							   </div>

							   <div class="col-md-4 col-sm-4">
								 <address>
								   <h1><b>Maranhão</b></h1>
								   <p>7034, Av. dos Holandeses, 6916 - Calhau</p>
								   <p>São Luís - MA</p>
								   <p>CEP: 65.071-380</p>
								 </address>
							   </div>
							   
						   </div>
						   </div>
                         </div>
                    </div>

               </div>
          </div>
     </section>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
