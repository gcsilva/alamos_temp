<?php

/* @var $this yii\web\View */

$this->title = 'Alamos Seguros';
?>
<!-- PRE LOADER -->
     <section class="preloader">
          <div class="spinner">

               <span class="spinner-rotate"></span>
               
          </div>
     </section>


     <!-- HOME -->
     <section id="home">
        <div class="overlay"></div>
          <div class="container">
               <div class="row">

                    <div class="col-md-12 col-sm-12">
                         <div class="home-info">
                              <h1>We are getting ready <br>to launch new CREATIVE site!</h1>
                              <!-- You can change the date time in init.js file -->
                              <ul class="countdown">
                                   <li>
                                        <span class="days">14</span>
                                        <h3>Days</h3>
                                   </li>
                                   <li>
                                        <span class="hours">10</span>
                                        <h3>hours</h3>
                                   </li>
                                   <li>
                                        <span class="minutes">15</span>
                                        <h3>minutes</h3>
                                   </li>
                                   <li>
                                        <span class="seconds">34</span>
                                        <h3>seconds</h3>
                                   </li>     
                              </ul>
                              <div class="subscribe-form">
                                <form action="" method="get">
                                  <input type="email" name="email" class="form-control" placeholder="Enter your email" required="">
                                  <button type="submit" class="form-control"><i class="fa fa-envelope-o"></i></button>
                                </form>
                              </div>
                         </div>
                    </div>

               </div>
          </div>
     </section>
